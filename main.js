$(function () {
    $('#dateInput').val(new Date().toLocaleDateString());

    // constants and global variables -------------------------
    const apiUrl = 'http://www.cbr.ru/scripts';
    const currencyCodes = {
        USD: "R01235",
        EUR: "R01239",
    }
    let currencyCourses = {
        RUB: '1.00'
    } // will look like currencyCodes object after setCurrencies() request

    let tabControls = $('.tab-control');
    let currentTabControl = $('.tab-control.active');
    let tabContent = $('.tab-content');
    let periodSelect = $('#periodSelect');
    let dateInput = $('#dateInput');
    let firstCurrencyInput = $('#firstCurrencyInput');
    let secondCurrencyInput = $('#secondCurrencyInput');
    let firstCurrencySelect = $('#firstCurrencySelect');
    let secondCurrencySelect = $('#secondCurrencySelect');

    let currentCurrencyCode = currencyCodes.USD;
    let currentPeriodName = 'month';

    // initial requests -------------------------
    setChart(currentCurrencyCode, currentPeriodName);
    setCurrencies(new Date()).then(() => convert());

    dateInput.datepicker({
        format: 'dd.mm.yyyy',
        endDate: "0d",
        language: "ru"
    });

    dateInput.on('changeDate', function (event) {
        let date = dateInput.datepicker('getDate');
        setCurrencies(new Date(date)).then(() => convert());
    })

    // set event handlers -------------------------
    tabControls.each(function (index) {
        let tabControl = $(this);

        tabControl.click(function (event) {
            if (!tabControl.is(currentTabControl)) {
                currentTabControl.removeClass('active');
                currentTabControl = tabControl;
                currentTabControl.addClass('active');

                currentCurrencyCode = currencyCodes[tabControl.children()[0].textContent];
                currentPeriodName = periodSelect.val();

                setChart(currentCurrencyCode, currentPeriodName);
            }
        })
    });

    periodSelect.change(function (event) {
        let self = $(this);
        if (self.val() !== currentPeriodName) {
            currentPeriodName = self.val();
            setChart(currentCurrencyCode, currentPeriodName);
        }
    })

    firstCurrencyInput.change(function (event) {
        let self = $(this);
        let result = convertToCurrencyFormat(self.val());

        self.val(result)
        convert();
    });

    firstCurrencySelect.change(function (event) {
        convert();
    });

    secondCurrencySelect.change(function (event) {
        convert();
    });

    // functions -------------------------
    function setChart(currencyCode, periodName) {
        return $.get(createUrl(currencyCode, periodName)).then(data => {
            console.log('data', data);
            return fillChart(data);
        });
    }

    function createUrl(currencyCode, periodName) {
        let period = getPeriod(periodName);
        return apiUrl + '/XML_dynamic.asp?date_req1=' + period.from + '&date_req2=' + period.to + '&VAL_NM_RQ=' + currencyCode;
    }

    function getPeriod(periodName) {
        let today = new Date();
        let from;

        const periodStarts = {
            week: new Date(today.getFullYear(), today.getMonth(), today.getDate() - 7).toLocaleDateString(),
            month: new Date(today.getFullYear(), today.getMonth() - 1, today.getDate()).toLocaleDateString(),
            halfyear: new Date(today.getFullYear(), today.getMonth() - 6, today.getDate()).toLocaleDateString(),
            year: new Date(today.getFullYear() - 1, today.getMonth(), today.getDate()).toLocaleDateString(),
        }

        if (periodName === 'week') {
            from = new Date(today.getFullYear(), today.getMonth(), today.getDate() - 7);
        } else if (periodName === 'month') {
            from = new Date(today.getFullYear(), today.getMonth() - 1, today.getDate());
        } else if (periodName === 'halfyear') {
            from = new Date(today.getFullYear(), today.getMonth() - 6, today.getDate());
        } else if (periodName === 'year') {
            from = new Date(today.getFullYear() - 1, today.getMonth(), today.getDate());
        } else {
            from = new Date(today.getFullYear(), today.getMonth() - 1, today.getDate());
        }

        return {
            from: from.toLocaleDateString(),
            to: today.toLocaleDateString()
        }
    }

    function getObjectsFromXML(xml) {
        let nodeList = xml.getElementsByTagName('Record');
        // conver NodeList ot array for more responsibility
        let array = Array.prototype.slice.call(nodeList);

        return array.map(element => {

            let date = element.getAttribute('Date')
            let value = element.getElementsByTagName('Value')[0].textContent;

            return {
                date: date,
                value: parseFloat(value)
            }
        });
    }

    function fillChart(xml) {
        let array = getObjectsFromXML(xml);

        let ctx = document.getElementById("myChart").getContext('2d');
        let data = {
            labels: array.map(e => e.date),
            datasets: [{
                data: array.map(e => e.value),
                borderWidth: 1
            }]
        };
        let options = {
            legend: {
                display: false
            },
            layout: {
                padding: {
                    left: 30,
                    right: 30
                }
            },
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: false
                    }
                }]
            }
        };
        let myChart = new Chart(ctx, {
            type: 'line',
            data: data,
            options: options
        });
    }

    function convertToCurrencyFormat(arg) {
        let result = parseFloat(arg.toString().replace(/,/g, ""))
            .toFixed(2)
            .toString()
            .replace(/\B(?=(\d{3})+(?!\d))/g, ",")
        return result !== 'NaN' ? result : '0.00';
    }

    function setCurrencies(date) {
        return $.get(apiUrl + '/XML_daily.asp?date_req=' + new Date(date).toLocaleDateString())
            .then(data => {
                let result = {};
                let arrayOfNodes = Array.prototype.slice.call(data.getElementsByTagName('Valute'));

                arrayOfNodes.forEach(elem => {
                    let charCode = elem.getElementsByTagName('CharCode')[0].textContent;
                    if (currencyCodes[charCode]) {
                        result[charCode] = elem.getElementsByTagName('Value')[0].textContent.replace(',', '.')
                    }
                });

                return Object.assign(currencyCourses, result);
            })
    }

    function convert() {
        let firstCourse = parseFloat(currencyCourses[firstCurrencySelect.val()]);
        let secondCourse = parseFloat(currencyCourses[secondCurrencySelect.val()]);
        let value = parseFloat(firstCurrencyInput.val());

        secondCurrencyInput.val(convertToCurrencyFormat(value * firstCourse / secondCourse));
    }

});